# terraml-commons

terraml-commons is an Open Source Java library for common utilities.

**Status**

1.0.0-SNAPSHOT

**Where can I get the latest release?**

Check our latest snapshot: https://oss.sonatype.org/#nexus-search;quick~online.terraml.commons

```
<repositories>
     <repository>
         <id>oss-sonatype</id>
         <name>oss-sonatype</name>
         <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
         <snapshots>
             <enabled>true</enabled>
         </snapshots>
     </repository>
 </repositories>

 <dependencies>
    <dependency>
      <groupId>online.terraml.commons</groupId>
      <artifactId>terraml-commons</artifactId>
      <version>1.0.0-SNAPSHOT</version>
    </dependency>
 </dependencies>
```

or

```
git clone https://bitbucket.org/terrayazilim/terraml-commons.git
```

or

```
https://bitbucket.org/terrayazilim/terraml-commons/get/249b2a98eb8b.zip
```
**Documentation**

* https://bitbucket.org/terrayazilim/terraml-commons/wiki

**Issue Management**

* https://bitbucket.org/terrayazilim/terraml-commons/issues

**Licence**

* Apache Licence Version 2.0, https://www.apache.org/licenses/LICENSE-2.0

**Contact**

* www.terrayazilim.com.tr
* arge@terrayazilim.com.tr
* info@terrayazilim.com.tr
