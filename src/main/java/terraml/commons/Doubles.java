/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons;

import java.util.Arrays;
import java.util.stream.DoubleStream;
import static terraml.commons.Objects.isNull;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @author Nazım Bahadır Şahin - bahadirbardakci@terrayazilim.com.tr
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Doubles {

    private Doubles() {
    }

    /**
     * @param double[]
     * @param double
     * @return index of given second arguments in the given first argument array. If second argument is not contained
     * in the array, then it returns -1.
     */
    public static int indexOf(final double[] array, final double element) {
        final int length = array.length;
        
        for (int t = 0; t < length; t++) {
            if (Double.compare(array[t], element) == 0) {
                return t;
            }
        }

        return -1;
    }

    /**
     * @param double[]
     * @param double
     * @return true if given element contained in the given array. Otherwise false.
     */
    public static boolean contains(final double[] array, final double element) {
        final int length = array.length;
        for (int t = 0; t < length; t++) {
            if (Double.compare(array[t], element) == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param double[]
     * @return Index of greatest element in the given array. If something goes wrong, it returns -1.
     */
    public static int findGreatestIndex(final double[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (Double.compare(array[t], array[found]) > 0) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param double[]
     * @return Index of smallest element in the given array. If something goes wrong, it returns -1.
     */
    public static int findSmallestIndex(final double[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (Double.compare(array[t], array[found]) < 0) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param double[]
     * @param double
     * @return A new array created by given array plus given element.
     */
    public static double[] add(final double[] array, final double element) {
        final int length = array.length;
        final double[] newArray = new double[length + 1];

        System.arraycopy(array, 0, newArray, 0, length);

        newArray[length] = element;
        return newArray;
    }

    /**
     * @param double[]
     * @return true if given array is either null or empty. Otherwise false.
     */
    public static boolean isEmpty(final double[] array) {
        return isNull(array) || (array.length == 0);
    }

    /**
     * @throws IllegalArgumentException if index is invalid.
     * @param double[]
     * @param int
     * @return A new array created from given array minus element that specified with given index.
     */
    public static double[] remove(final double[] array, final int index) {
        final int length = array.length;
        if ((index > length) || (index < 0)) {
            throw new IllegalArgumentException("index is invalid");
        }

        final double[] newArray = new double[length - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        if (index > length - 1) {
            return newArray;
        }

        final int destination = length - index - 1;
        System.arraycopy(array, index + 1, newArray, index, destination);

        return newArray;
    }

    /**
     * @throws IllegalArgumentException if given element is not contained in the given array.
     * @param double[]
     * @param double
     * @return A new array created from given array minus given element.
     */
    public static double[] remove(final double[] array, double element) {
        final int idx = indexOf(array, element);
        if (idx == -1) {
            throw new IllegalArgumentException("there is no such element in the given array.");
        }

        return remove(array, idx);
    }

    /**
     * @param double[]
     * @return Greatest element in the given array. If something goes wrong, then it returns -1.
     */
    public static double findGreatest(final double[] array) {
        double found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.max(array[t], found);
        }

        return found;
    }

    /**
     * @param double[]
     * @return Smallest element in the given array. If something goes wrong, then it returns -1.
     */
    public static double findSmallest(final double[] array) {
        double found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.min(array[t], found);
        }

        return found;
    }

    /**
     * @param double[]
     * @return Sorted version of given array.
     */
    public static double[] sort(final double[] source) {
        return DoubleStream.of(source).sorted().toArray();
    }

    /**
     * @param double[]
     * @param double[]
     * @return
     */
    public static double[] removeAll(final double[] source, final double[] rmv) {
        return Arrays.stream(rmv)
                .filter(each -> !contains(source, each))
                .toArray();
    }
    
    /**
     * @param double
     * @param double
     * @return true if given two arguments are numerically equal. Otherwise false.
     */
    public static final boolean isEqual(final double number0, final double number1) {
        return Double.compare(number0, number1) == 0;
    }

    /**
     * @param double
     * @param double
     * @return true if given first argument is numerically greater than the second one. Otherwise false.
     */
    public static final boolean isGreater(final double number0, final double number1) {
        return Double.compare(number0, number1) > 0;
    }

    /**
     * @param double
     * @param double
     * @return true if given first argument is numerically greater or equal than the second one. Otherwise false.
     */
    public static final boolean isGreaterEqual(final double number0, final double number1) {
        return Double.compare(number0, number1) >= 0;
    }

    /**
     * @param double
     * @param double
     * @return true if given first argument is numerically smaller than the second one. Otherwise false.
     */
    public static final boolean isSmaller(final double number0, final double number1) {
        return Double.compare(number0, number1) < 0;
    }

    /**
     * @param number0
     * @param number1
     * @return true if given first argument is numerically smaller or equal than the second one. Otherwise false.
     */
    public static final boolean isSmallerEqual(final double number0, final double number1) {
        return Double.compare(number0, number1) <= 0;
    }

    /**
     * @param double
     * @return true if given argument is numerically smaller than 0.0. Otherwise false.
     */
    public static final boolean isNegative(final double number0) {
        return Double.compare(number0, 0f) < 0;
    }

    /**
     * @param double
     * @return true if given argument is numerically greater than 0.0. Otherwise false.
     */
    public static final boolean isPositive(final double number0) {
        return Double.compare(number0, 0f) > 0;
    }

    /**
     * EPSILON: 1E-8
     *
     * @param double
     * @param double
     * @return ABSOLUTE(number0 - number1) < EPSILON
     */
    public static final boolean isNumericallyEqual(final double number0, final double number1) {
        return Double.compare(Math.abs(number0 - number1), 1E-8) < 0;
    }
    
    /**
     * @param double
     * @param double
     * @param double
     * @return ABSOLUTE(number0 - number1) < epsilon
     */
    public static final boolean isNumericallyEqual(final double number0, final double number1, final double epsilon) {
        return Double.compare(Math.abs(number0 - number1), epsilon) < 0;
    }
}
