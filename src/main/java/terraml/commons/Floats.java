/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons;

import static terraml.commons.Objects.isNull;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Floats {

    private Floats() {
    }

    /**
     * @param float[]
     * @param float
     * @return
     */
    public static int indexOf(final float[] array, final float element) {
        final int length = array.length;
        for (int t = 0; t < length; t++) {
            if (Float.compare(array[t], element) == 0) {
                return t;
            }
        }

        return -1;
    }

    /**
     * @param float[]
     * @param float
     * @return
     */
    public static boolean contains(final float[] array, final float element) {
        final int length = array.length;
        for (int t = 0; t < length; t++) {
            if (Float.compare(array[t], element) == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param float[]
     * @return
     */
    public static int findGreatestIndex(final float[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (Float.compare(array[t], array[found]) > 0) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param float[]
     * @return
     */
    public static int findSmallestIndex(final float[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (Float.compare(array[t], array[found]) < 0) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param float[]
     * @param float
     * @return
     */
    public static float[] add(final float[] array, final float element) {
        final int length = array.length;
        final float[] newArray = new float[length + 1];

        System.arraycopy(array, 0, newArray, 0, length);

        newArray[length] = element;
        return newArray;
    }

    /**
     * @param float[]
     * @return
     */
    public static boolean isEmpty(final float[] array) {
        return isNull(array) || (array.length == 0);
    }

    /**
     * @param float
     * @param int
     * @return
     */
    public static float[] remove(final float[] array, final int index) {
        final int length = array.length;
        if ((index > length) || (index < 0)) {
            throw new IllegalArgumentException("index is invalid");
        }

        final float[] newArray = new float[length - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        if (index > length - 1) {
            return newArray;
        }

        final int destination = length - index - 1;
        System.arraycopy(array, index + 1, newArray, index, destination);

        return newArray;
    }

    /**
     * @param float[]
     * @param float
     * @return
     */
    public static float[] remove(final float[] array, float element) {
        final int idx = indexOf(array, element);
        if (idx == -1) {
            throw new IllegalArgumentException("there is no such element in the given array.");
        }

        return remove(array, idx);
    }

    /**
     * @param float[]
     * @return
     */
    public static float findGreatest(final float[] array) {
        float found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.max(array[t], found);
        }

        return found;
    }

    /**
     * @param float[]
     * @return
     */
    public static float findSmallest(final float[] array) {
        float found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.min(array[t], found);
        }

        return found;
    }
    
    /**
     * @param float
     * @param float
     * @return
     */
    public static boolean isEqual(final float number0, final float number1) {
        return Float.compare(number0, number1) == 0;
    }

    /**
     * @param float
     * @param float
     * @return
     */
    public static boolean isGreater(final float number0, final float number1) {
        return Float.compare(number0, number1) > 0;
    }

    /**
     * @param float
     * @param float
     * @return
     */
    public static boolean isGreaterEqual(final float number0, final float number1) {
        return Float.compare(number0, number1) >= 0;
    }

    /**
     * @param float
     * @param float
     * @return
     */
    public static boolean isSmaller(final float number0, final float number1) {
        return Float.compare(number0, number1) < 0;
    }

    /**
     * @param float
     * @param float
     * @return
     */
    public static boolean isSmallerEqual(final float number0, final float number1) {
        return Float.compare(number0, number1) <= 0;
    }

    /**
     * @param float
     * @return
     */
    public static boolean isNegative(final float number0) {
        return Float.compare(number0, 0f) < 0;
    }

    /**
     * @param float
     * @return
     */
    public static boolean isPositive(final float number0) {
        return Float.compare(number0, 0f) > 0;
    }
}
