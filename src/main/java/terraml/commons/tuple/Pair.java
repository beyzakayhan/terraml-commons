/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons.tuple;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Pair<K, V> implements Serializable {

    public final K key;
    public final V value;

    /**
     * @param key
     * @param value 
     */
    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
    
    /**
     * @param <K>
     * @param <V>
     * @param key
     * @param value
     * @return 
     */
    public static <K, V> Pair<K, V> of(K key, V value) {
        return new Pair<>(key, value);
    } 

    /**
     * @return 
     */
    public K getKey() {
        return key;
    }

    /**
     * @return 
     */
    public V getValue() {
        return value;
    }

    @Override
    public Pair<K, V> clone() {
        return new Pair<>(getKey(), getValue());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.key);
        hash = 67 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pair<?, ?> other = (Pair<?, ?>) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
}
