/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons.tuple;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class NativeEntry<K, V> implements Map.Entry<K, V>, Serializable {

    private final K key;
    private V value;

    /**
     * @param key
     * @param value 
     */
    public NativeEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }
    
    /**
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> NativeEntry<K, V> of(K key, V value) {
        return new NativeEntry<>(key, value);
    }
    
    @Override
    public K getKey() {
        return this.key;
    }

    @Override
    public V getValue() {
        return this.value;
    }

    @Override
    public V setValue(V value) {
        V reference = this.value;
        this.value = value;
        
        return reference;
    }

    @Override
    public NativeEntry<K, V> clone() {
        return new NativeEntry<>(getKey(), getValue());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.key);
        hash = 47 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NativeEntry<?, ?> other = (NativeEntry<?, ?>) obj;
        if (!Objects.equals(this.key, other.key)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
    
}
