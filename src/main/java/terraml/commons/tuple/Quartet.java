/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons.tuple;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Quartet<K, L, M, N> implements Serializable {
    
    public final K k;
    public final L l;
    public final M m;
    public final N n;

    /**
     * @param k
     * @param l
     * @param m
     * @param n 
     */
    public Quartet(K k, L l, M m, N n) {
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
    }

    /**
     * @param <K>
     * @param <L>
     * @param <M>
     * @param <N>
     * @param k
     * @param l
     * @param m
     * @param n
     * @return 
     */
    public static <K, L, M, N> Quartet<K, L, M, N> of(K k, L l, M m, N n) {
        return new Quartet<>(k, l, m, n);
    }
    
    /**
     * @return 
     */
    public K getK() {
        return k;
    }

    /**
     * @return 
     */
    public L getL() {
        return l;
    }

    /**
     * @return 
     */
    public M getM() {
        return m;
    }

    /**
     * @return 
     */
    public N getN() {
        return n;
    }

    @Override
    protected Quartet<K, L, M, N> clone() {
        return new Quartet<>(getK(), getL(), getM(), getN());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.k);
        hash = 83 * hash + Objects.hashCode(this.l);
        hash = 83 * hash + Objects.hashCode(this.m);
        hash = 83 * hash + Objects.hashCode(this.n);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Quartet<?, ?, ?, ?> other = (Quartet<?, ?, ?, ?>) obj;
        if (!Objects.equals(this.k, other.k)) {
            return false;
        }
        if (!Objects.equals(this.l, other.l)) {
            return false;
        }
        if (!Objects.equals(this.m, other.m)) {
            return false;
        }
        if (!Objects.equals(this.n, other.n)) {
            return false;
        }
        return true;
    }

}
