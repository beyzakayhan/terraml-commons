/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons.math;

import static terraml.commons.Doubles.isEqual;
import static terraml.commons.Doubles.isGreaterEqual;
import static terraml.commons.Doubles.isSmaller;
import static terraml.commons.Doubles.isSmallerEqual;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Angle implements Comparable<Angle> {

    public final double degree;
    public final double radian;

    /**
     * @param double 
     */
    private Angle(double degree) {
        this.degree = degree;
        this.radian = Math.toRadians(this.degree);
    }

    /**
     * @param Angle
     */
    public Angle(Angle angle) {
        this.degree = angle.degree;
        this.radian = angle.radian;
    }

    /**
     * @param double
     * @return
     */
    public static Angle fromDegree(final double degree) {
        return new Angle(degree);
    }

    /**
     * @param double
     * @return
     */
    public static Angle fromRadian(final double radian) {
        return new Angle(Math.toDegrees(radian));
    }

    /**
     * @param Angle
     * @return
     */
    public Angle add(Angle angle) {
        return Angle.fromDegree(this.degree + angle.degree);
    }

    /**
     * @param Angle
     * @return
     */
    public Angle sub(Angle angle) {
        return Angle.fromDegree(this.degree - angle.degree);
    }

    /**
     * @param Angle
     * @return
     */
    public Angle mul(Angle angle) {
        return Angle.fromDegree(this.degree * angle.degree);
    }

    /**
     * @param Angle
     * @return
     */
    public Angle div(Angle angle) {
        return Angle.fromDegree(this.degree / angle.degree);
    }

    /**
     * @param Angle
     * @return
     */
    public Angle min(Angle angle) {
        if (isSmallerEqual(compareTo(angle), 0d)) {
            return this;
        } else {
            return new Angle(angle);
        }
    }

    /**
     * @param Angle
     * @return
     */
    public Angle max(Angle angle) {
        if (isGreaterEqual(compareTo(angle), 0d)) {
            return this;
        } else {
            return new Angle(angle);
        }
    }

    /**
     * @return
     */
    public double sin() {
        return Math.sin(this.radian);
    }

    /**
     * @return
     */
    public double asin() {
        return Math.asin(this.radian);
    }

    /**
     * @return
     */
    public double cos() {
        return Math.cos(this.radian);
    }

    /**
     * @return
     */
    public double acos() {
        return Math.acos(this.radian);
    }

    /**
     * @return
     */
    public double atan() {
        return Math.atan(this.radian);
    }

    /**
     * @return
     */
    public double getRadian() {
        return this.radian;
    }

    /**
     * @return
     */
    public double getDegree() {
        return this.degree;
    }

    @Override
    public int compareTo(Angle angle) {
        if (isSmaller(this.radian, angle.degree)) {
            return -1;
        } else if (isEqual(this.radian, angle.degree)) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.degree) ^ (Double.doubleToLongBits(this.degree) >>> 32));
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.radian) ^ (Double.doubleToLongBits(this.radian) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Angle other = (Angle) obj;
        if (Double.doubleToLongBits(this.degree) != Double.doubleToLongBits(other.degree)) {
            return false;
        }
        if (Double.doubleToLongBits(this.radian) != Double.doubleToLongBits(other.radian)) {
            return false;
        }
        return true;
    }
}
