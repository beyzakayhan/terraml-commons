/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons.math;

import static terraml.commons.Floats.isEqual;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class RationalNumber implements Comparable<RationalNumber> {

    public final float nominator; // pay
    public final float denominator; // payda

    /**
     * @param float
     * @param float
     */
    public RationalNumber(float nominator, float denominator) {
        if (isEqual(denominator, 0)) {
            throw new IllegalStateException("denominator is zero");
        }

        float gcd = gcd(nominator, denominator);

        this.denominator = denominator / gcd;
        this.nominator = nominator / gcd;
    }

    /**
     * @param RationalNumber 
     */
    public RationalNumber(RationalNumber rNumber) {
        this(rNumber.nominator, rNumber.denominator);
    }

    /**
     * @param float
     * @param float
     * @return
     */
    public static RationalNumber of(float pay, float payda) {
        return new RationalNumber(pay, payda);
    }

    /**
     * @param float
     * @param float
     * @return
     */
    public static float gcd(final float m, final float n) {
        final float first = Math.abs(m);
        final float second = Math.abs(n);

        if (second == 0) {
            return first;
        }

        return gcd(second, first % second);
    }

    /**
     * @param float
     * @param float
     * @return
     */
    public static float lcm(final float m, final float n) {
        final float first = Math.abs(m);
        final float second = Math.abs(n);

        return first * (second / gcd(first, second));
    }

    /**
     * @param RationalNumber
     * @return
     */
    public RationalNumber mul(RationalNumber rational) {
        final float newNom = getNominator() * rational.getNominator();
        final float newDenom = getDenominator() * rational.getDenominator();

        return new RationalNumber(newNom, newDenom);
    }

    /**
     * @param RationalNumber
     * @return
     */
    public RationalNumber add(RationalNumber rational) {
        if (isEqual(rational.nominator, 0)) {
            return this;
        }

        final float gcd_nom = gcd(getNominator(), rational.getNominator());
        final float gcd_denom = gcd(getDenominator(), rational.getDenominator());

        float formula = (getNominator() / gcd_nom) * (rational.getDenominator() * gcd_denom);
        formula += (rational.getNominator() / gcd_nom) * (getDenominator() / gcd_denom);

        final float newNominator = new RationalNumber(formula, lcm(getDenominator(), rational.getDenominator())).getNominator();
        float newDenominator = newNominator * gcd_nom;

        return new RationalNumber(newNominator, newDenominator);
    }

    /**
     * @param RationalNumber
     * @return
     */
    public RationalNumber sub(RationalNumber rational) {
        final RationalNumber ref = new RationalNumber(-1 * rational.getNominator(), rational.getDenominator());

        return add(ref);
    }

    /**
     * @param RationalNumber
     * @return
     */
    public RationalNumber div(RationalNumber rational) {
        RationalNumber inverse = RationalNumber.of(rational.getDenominator(), rational.getNominator());

        return mul(inverse);
    }

    /**
     * @return
     */
    public double toDouble() {
        return Double.valueOf(this.nominator / this.denominator);
    }

    /**
     * @return
     */
    public float toFloat() {
        return this.nominator / this.denominator;
    }

    /**
     * @return
     */
    public int toInt() {
        return Math.round(this.nominator / this.denominator);
    }

    /**
     * @return
     */
    public float getDenominator() {
        return denominator;
    }

    /**
     * @return
     */
    public float getNominator() {
        return nominator;
    }

    @Override
    public int compareTo(RationalNumber o) {
        return Float.compare(toFloat(), o.toFloat());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Float.floatToIntBits(this.nominator);
        hash = 23 * hash + Float.floatToIntBits(this.denominator);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RationalNumber other = (RationalNumber) obj;
        if (Float.floatToIntBits(this.nominator) != Float.floatToIntBits(other.nominator)) {
            return false;
        }
        if (Float.floatToIntBits(this.denominator) != Float.floatToIntBits(other.denominator)) {
            return false;
        }
        return true;
    }
}
