/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons.math;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static terraml.commons.Doubles.isGreaterEqual;
import static terraml.commons.Doubles.isSmallerEqual;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @author Nazım Bahadır Bardakcı - bahadirbardakci@terrayazilim.com.tr
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Interval {

    public final double left; // lower
    public final double right; // greater

    
    /**
     * @param left
     * @param right 
     */
    public Interval(double left, double right) {
        this.left = min(left, right);
        this.right = max(left, right);
    }

    /**
     * @param Interval
     */
    public Interval(Interval interval) {
        this.left = interval.left;
        this.right = interval.right;
    }

    /**
     * @param double
     * @param double
     * @return
     */
    public static Interval of(final double left, final double right) {
        return new Interval(left, right);
    }

    /**
     * @param double
     * @param double
     * @param double
     * @param double
     * @return if double parameters contains between variables it returns true,
     * otherwise it returns false
     */
    public static final boolean contains(double Ql, double Qr, double Wl, double Wr) {
        return _contains(Ql, Qr, Wl, Wr);
    }

    /**
     * @param double
     * @param double
     * @param double
     * @return any variable in the range it returns true, otherwise it returns
     * false
     */
    public static final boolean inRange(double Ql, double Qr, double W) {
        return _inRange(Ql, Qr, W);
    }

    /**
     * @param double
     * @param double
     * @param double
     * @param double
     * @return if data variable have any intersections it returns true ,
     * otherwise it returns false
     */
    public static final boolean intersects(double Ql, double Qr, double Wl, double Wr) {
        return _intersects(Ql, Qr, Wl, Wr);
    }

    /**
     * @param double
     * @param double
     * @param double
     * @param double
     * @return it is checking is numbers has intersection or not . If has any
     * intersection between values it returns true, otherwise it returns false
     */
    public static final Interval intersection(double Ql, double Qr, double Wl, double Wr) {
        return _intersection(Ql, Qr, Wl, Wr);
    }

    /**
     * @param double
     * @param double
     * @param double
     * @param double
     * @return _contains checking isGreaterEquall && isSmallerEqual methods and
     * both of them must be true for it returns true, otherwise it returns false
     */
    private static boolean _contains(double Ql, double Qr, double Wl, double Wr) {
        return isGreaterEqual(Wl, Ql) && isSmallerEqual(Wr, Qr);
    }

    /**
     * @param double
     * @param double
     * @param double
     * @return _inRange checking isGreaterEquall && isSmallerEqual methods and
     * both of them must be true for it returns true, otherwise it returns false
     */
    private static boolean _inRange(double Ql, double Qr, double W) {
        return isGreaterEqual(W, Ql) && isSmallerEqual(W, Qr);
    }

    /**
     * @param double
     * @param double
     * @param double
     * @param double
     * @return we cannot reach from outside for JUnit tests...
     */
    private static boolean _intersects(double Ql, double Qr, double Wl, double Wr) {
        return isSmallerEqual(max(Ql, Wl), min(Qr, Wr));
    }

    /**
     * @param double
     * @param double
     * @param double
     * @param double
     * @return We cannot reach from outside for JUnit tests...
     */
    private static Interval _intersection(double Ql, double Qr, double Wl, double Wr) {
        final double newLeft = max(Ql, Wl);
        final double newRight = min(Qr, Wr);

        return new Interval(newLeft, newRight);
    }

    /**
     * @param Interval
     * @return if left and right variables is in the any contain line it returns
     * true , otherwise it returns false.
     */
    public boolean contains(Interval interval) {
        return _contains(left, right, interval.getLeft(), interval.getRight());
    }

    /**
     * @param double
     * @param double
     * @return if any members of ori or dst including contains it returns true,
     * otherwise it returns false.
     */
    public boolean contains(double ori, double dst) {
        return _contains(left, right, min(ori, dst), max(ori, dst));
    }

    /**
     * @param double
     * @return looking for the members inRange or not if in the range it returns
     * true, otherwise returns false.
     */
    public boolean inRange(double related) {
        return _inRange(left, right, related);
    }

    /**
     * @param Interval
     * @return getting interval variables left and right for the check are there
     * any intersects. If answer is yes it returns true , otherwise it returns
     * false .
     */
    public boolean intersects(Interval interval) {
        return _intersects(left, right, interval.getLeft(), interval.getRight());
    }

    /**
     * @param double
     * @param double
     * @return looking min and max variables where is added left and right
     * variables it is checking are there any intersects or not . If there is an
     * intersects returns true otherwise returns false .
     */
    public boolean intersects(double ori, double dst) {
        return _intersects(left, right, min(ori, dst), max(ori, dst));
    }

    /**
     * @param Interval
     * @return Getting interval left and right variables and looking for
     * intersection if variables have any intersection it can show us.
     */
    public Interval intersection(Interval interval) {
        return _intersection(left, right, interval.getLeft(), interval.getRight());
    }

    /**
     * @param double
     * @param double
     * @return setting variables that are ori and dst then it looks min and max
     * results for left and right. If there are any intersection point it
     * returns true , otherwise it returns false
     */
    public Interval intersection(double ori, double dst) {
        return _intersection(left, right, min(ori, dst), max(ori, dst));
    }

    /**
     * @param double
     * @return left variable
     */
    public double getLeft() {
        return left;
    }

    /**
     * @param double
     * @return right variable
     */
    public double getRight() {
        return right;
    }

    /**
     * @return Finding middle of the line.
     */
    public double getMiddle() {
        return (left + right) * 0.5;
    }

    /**
     * @return Finding length of the line.
     */
    public double getLength() {
        return right - left;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.left) ^ (Double.doubleToLongBits(this.left) >>> 32));
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.right) ^ (Double.doubleToLongBits(this.right) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Interval other = (Interval) obj;
        if (Double.doubleToLongBits(this.left) != Double.doubleToLongBits(other.left)) {
            return false;
        }
        if (Double.doubleToLongBits(this.right) != Double.doubleToLongBits(other.right)) {
            return false;
        }
        return true;
    }
    
}
