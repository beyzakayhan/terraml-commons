/*
 * This file is part of terraml-commons project.
 *
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package terraml.commons;

import java.util.Objects;
import java.util.SortedMap;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @author Salih Sarı - salihsari@terrayazilim.com.tr
 * @author Berat Eke - berateke@terrayazilim.com.tr
 * @version 1.0.0-SNAPSHOT
 */
public final class JSBuilder {

    private static final String ILLEGAL_CHARS = "₺\"!>'£^#+$%½&¾/{([)]=}?\\*|"
            + "-<@€¶:←ûîô'ª§â«»¢“”µ×·,´ığĞüÜşŞİöÖçÇ ";

    private static final String[] RESERVED_KEYWORDS = {"abstract", "arguments", "await*",
                                                       "boolean", "break", "byte", "case", "catch", "char", "class*", "const",
                                                       "continue", "debugger", "default", "delete", "do", "double", "else",
                                                       "enum*", "eval", "export*", "extends*", "false", "final", "finally",
                                                       "float", "for", "function", "goto", "if", "implements", "import*",
                                                       "in", "instanceof", "int", "interface", "let", "long", "native",
                                                       "new", "null", "package", "private", "protected", "public", "return",
                                                       "short", "static", "super*", "switch", "synchronized", "this",
                                                       "throw", "throws", "transient", "true", "try", "typeof", "var",
                                                       "void", "volatile", "while", "with", "yield"};

    private final JSFunctionModel model;
    private final StringBuilder jsCode;

    /**
     * @param jsm
     */
    public JSBuilder(JSFunctionModel jsm) {

        this.model = jsm;
        this.jsCode = new StringBuilder();

    }

    /**
     * @return
     */
    public boolean create() {

        if (getModel() instanceof JSFunction) {
            return createJSFunction();
        } else if (getModel() instanceof JSCallFunction) {
            return createJSFunctionCall();
        }

        return false;
    }

    /**
     * @return true if function name, content and parameters are valid. Otherwise false.
     */
    private boolean createJSFunction() {

        if (!isFunctionNameValid()) {

            return false;

        }

        if (!isParametersValid()) {

            return false;

        }

        if (!isContentValid()) {

            return false;

        }

        jsCode.append("function ")
                .append(getModel().getFunctionName())
                .append(" (")
                .append(addParameters(getModel().getParameters()))
                .append(") {")
                .append(((JSFunction) getModel()).getContent())
                .append("};");

        return true;

    }

    /**
     *
     * @return true if function name to call function is valid. Otherwise false.
     */
    private boolean createJSFunctionCall() {
        if (!isFunctionNameValid()) {

            return false;

        }

        if (!isObjValid()) {

            return false;

        }

        if (!isParametersValid()) {

            return false;

        }

        jsCode.append(((JSCallFunction) getModel()).getObj())
                .append(".")
                .append(getModel().getFunctionName())
                .append("(")
                .append(addParameters(getModel().getParameters()))
                .append(");");

        return true;
    }

    /**
     * @return
     */
    public boolean isObjValid() {

        String obj = ((JSCallFunction) getModel()).getObj();

        if (obj == null) {

            return false;

        }

        return isStringValid(obj);

    }

    /**
     * @return true if functionName is valid and not equals with blank. Otherwise false.
     */
    public boolean isFunctionNameValid() {

        String functionName = getModel().getFunctionName();

        if (functionName == null) {

            return false;

        }

        return isFunctionNameValid(getModel().getFunctionName());
    }

    /**
     *
     * @param variableName
     * @return true if the parameter is available in function and variable names. Oterwise false.
     */
    public boolean isParameterValid(Object variableName) {

        boolean isValid = true;

        // if parameter is not instance of String then it counts valid.
        if (variableName instanceof String) {
            String currentParameter = (String) variableName;

            // if it's JS String, then validation is not necessary.
            if (currentParameter.startsWith("'") && currentParameter.endsWith("'")) {
                return isValid;
            } else {
                if (!isStringValid(currentParameter)) {
                    isValid = false;
                }
            }

        }

        return isValid;
    }

    /**
     * @return true if parameter valid. Otherwise false.
     */
    public boolean isParametersValid() {

        boolean valid = true;

        // get parameters
        for ( Object arg : getModel().getParameters().values() ) {

            // check if the current parameter is valid
            if (!isParameterValid(arg)) {

                valid = false;

                break;

            }

        }

        return valid;

    }

    /**
     * @return true if content valid. Otherwise false.
     */
    public boolean isContentValid() {

        // does model have content
        if (!(getModel() instanceof JSFunction)) {
            return false;
        }

        String content = ((JSFunction) getModel()).getContent();

        return !(content == null || content.isEmpty());
    }

    /**
     * @return
     */
    public String getJsCode() {

        return jsCode.toString();

    }

    /**
     *
     * @param parameters
     * @return
     */
    private String addParameters(SortedMap<Integer, Object> parameters) {
        final StringBuilder params = new StringBuilder();

        // get parameters in order.
        for ( int index : parameters.keySet() ) {

            // prevent extra comma if the parameter is last one.
            if (index == parameters.lastKey()) {

                params.append(parameters.get(index));

            } else {

                // put parameter and comma.
                params.append(parameters.get(index))
                        .append(",");

            }

        }

        return params.toString();

    }

    /**
     * @param functionName
     * @return
     */
    private boolean isFunctionNameValid(String functionName) {
        if (!isFunctionsNameFirstCharValid(functionName)) {

            return false;

        }

        return isStringValid(functionName);

    }

    /**
     * @param functionName
     * @return
     */
    private boolean isFunctionsNameFirstCharValid(String functionName) {
        // pre-check for null and empty string
        if (functionName == null || functionName.isEmpty()) {
            return false;
        }

        return !functionName.matches("[0-9].*");
    }

    /**
     * @param variableName
     * @return true if the parameter does not contain invalid characters. Otherwise false.
     */
    private boolean isStringValid(String variableName) {

        boolean isValid = true;

        // pre-check for null and empty string
        if (variableName == null || variableName.isEmpty()) {
            return false;
        }

        // check for reserved Javascript keywords.
        for ( String rK : RESERVED_KEYWORDS ) {

            if (variableName.equals(rK)) {

                isValid = false;
                break;

            }

        }

        // check for illegal characters.
        for ( char ch : variableName.toCharArray() ) {

            if (ILLEGAL_CHARS.indexOf(ch) >= 0) {

                isValid = false;
                break;

            }

        }

        return isValid;

    }

    /**
     * @return
     */
    public JSFunctionModel getModel() {
        return model;
    }

    @Override
    public String toString() {
        return this.jsCode.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.model);
        hash = 61 * hash + Objects.hashCode(this.jsCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSBuilder other = (JSBuilder) obj;
        if (!Objects.equals(this.model, other.model)) {
            return false;
        }
        if (!Objects.equals(this.jsCode, other.jsCode)) {
            return false;
        }
        return true;
    }
}
