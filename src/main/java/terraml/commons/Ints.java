/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons;

import java.util.Arrays;
import java.util.stream.IntStream;
import static terraml.commons.Objects.isNull;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Ints {

    private Ints() {
    }


    /**
     * @param int[]
     * @param int
     * @return
     */
    public static int indexOf(final int[] array, final int element) {
        final int length = array.length;
        for (int t = 0; t < length; t++) {
            if (array[t] == element) {
                return t;
            }
        }

        return -1;
    }

    /**
     * @param int[]
     * @param int
     * @return
     */
    public static boolean contains(final int[] array, final int element) {
        final int length = array.length;
        for (int t = 0; t < length; t++) {
            if (array[t] == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return
     */
    public static int[] emptyArray() {
        return new int[0];
    }

    /**
     * @param int[]
     * @return
     */
    public static int findGreatestIndex(final int[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (array[t] > array[found]) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param int[]
     * @return
     */
    public static int findSmallestIndex(final int[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (array[t] < array[found]) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param int[]
     * @param int
     * @return
     */
    public static int[] add(final int[] array, final int element) {
        final int length = array.length;
        final int[] newArray = new int[length + 1];

        System.arraycopy(array, 0, newArray, 0, length);

        newArray[length] = element;
        return newArray;
    }

    /**
     * @param int[]
     * @return boolean
     */
    public static boolean isEmpty(final int[] array) {
        return isNull(array) || (array.length == 0);
    }

    /**
     * @param int[]
     * @param int
     * @return
     */
    public static int[] removeFromIndex(final int[] array, final int index) {
        final int length = array.length;
        if ((index > length) || (index < 0)) {
            throw new IllegalArgumentException("index is invalid");
        }

        final int[] newArray = new int[length - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        if (index > length - 1) {
            return newArray;
        }

        final int destination = length - index - 1;
        System.arraycopy(array, index + 1, newArray, index, destination);

        return newArray;
    }

    /**
     * @param int[]
     * @param int
     * @return
     */
    public static int[] remove(final int[] array, int element) {
        final int idx = indexOf(array, element);
        if (idx == -1) {
            throw new IllegalArgumentException("there is no such element in the given array.");
        }

        return removeFromIndex(array, idx);
    }

    /**
     * @param int[]
     * @return
     */
    public static int findGreatest(final int[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.max(array[t], found);
        }

        return found;
    }

    /**
     * @param int[]
     * @return
     */
    public static int findSmallest(final int[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.min(array[t], found);
        }

        return found;
    }

    /**
     * @param int[]
     * @return
     */
    public static int[] sort(final int[] source) {
        return IntStream.of(source).sorted().toArray();
    }

    /**
     * @param int[]
     * @param int[]
     * @return
     */
    public static int[] removeAll(final int[] source, final int[] rmv) {
        return Arrays.stream(rmv)
                .filter(each -> !contains(source, each))
                .toArray();
    }

    /**
     * @param int
     * @param int
     * @return
     */
    public static boolean isEqual(final int number0, final int number1) {
        return Integer.compare(number0, number1) == 0;
    }

    /**
     * @param int
     * @param int
     * @return
     */
    public static boolean isGreater(final int number0, final int number1) {
        return number0 > number1;
    }

    /**
     * @param int
     * @param int
     * @return
     */
    public static boolean isGreaterEqual(final int number0, final int number1) {
        return number0 >= number1;
    }

    /**
     * @param int
     * @param int
     * @return
     */
    public static boolean isSmaller(final int number0, final int number1) {
        return number0 < number1;
    }

    /**
     * @param int
     * @param int
     * @return
     */
    public static boolean isSmallerEqual(final int number0, final int number1) {
        return number0 <= number1;
    }

    /**
     * @param int
     * @return
     */
    public static boolean isNegative(final int number0) {
        return number0 < 0;
    }

    /**
     * @param int
     * @return
     */
    public static boolean isPositive(final int number0) {
        return number0 > 0;
    }

}
