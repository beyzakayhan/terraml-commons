/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @author Nazım Bahadır Bardakcı - bahadirbardakci@terrayazilim.com.tr
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Lists {

    public static final List EMPTY_LIST = Collections.emptyList();
    
    private Lists() {
    }

    /**
     * @param <Q>
     * @param Iterable<Q>
     * @return A new Arraylist iterated from given argument. If given argument is null, then it returns
     * empty ArrayList.
     */
    public static <Q> List<Q> toList(Iterable<Q> elements) {
        if (elements == null) {
            return new ArrayList<>();
        }
        
        final List<Q> arraylist = new ArrayList<>();
        elements.forEach(item -> arraylist.add(item));
        
        return arraylist;
    }
    
    /**
     *
     * @param <Q>
     * @param elements
     * @return A new Arraylist iterated from given argument. If given argument is null, then it returns
     * empty ArrayList.
     */
    public static <Q> List<Q> toList(Iterator<Q> elements) {
        if (elements == null) {
            return new ArrayList<>();
        }
        
        final List<Q> arraylist = new ArrayList<>();
        for (Iterator<Q> iterator = elements; iterator.hasNext();) {
            arraylist.add(iterator.next());
        }
        
        return arraylist;
    }
    
    /**
     * Method uses Stream.
     * 
     * @param <Q>
     * @param List<Q>
     * @param Comparator
     * @return Minimum <Q> object.
     */
    public static <Q> Q min(List<Q> list, Comparator<Q> comparator) {
        return list.stream().min(comparator).get();
    }

    /**
     * Method uses Stream.
     * 
     * @param <Q>
     * @param List<Q>
     * @param Comparator<Q>
     * @return Maximum <Q> object.
     */
    public static <Q> Q max(List<Q> list, Comparator<Q> comparator) {
        return list.stream().max(comparator).get();
    }
    
    /**
     * Method uses Stream.
     * 
     * @param <Q>
     * @param List<Q>
     * @param Predicate<Q>
     * @return Selected <Q> objects.
     */
    public static <Q> List<Q> select(List<Q> list, Predicate<Q> predicate) {
        return list.stream().filter(predicate).collect(Collectors.toList());
    }
    
    /**
     * Method uses Stream.
     * 
     * @param <Q>
     * @param List<Q>
     * @param Predicate<Q>
     * @return Rejected <Q> objects.
     */
    public static <Q> List<Q> reject(List<Q> list, Predicate<Q> predicate) {
        return list.stream().filter(predicate.negate()).collect(Collectors.toList());
    }
    
    /**
     * @param <Q>
     * @param list
     * @return Deep copied new ArrayList from given arg List.
     */
    public static <Q> List<Q> deepCopy(List<Q> list) throws IOException, ClassNotFoundException {
        List<Q> dCopy = new ArrayList<>(list.size());
        
        for ( Q each : list) {
            dCopy.add(Objects.deepCopy(each));
        }
        
        return dCopy;
    }
    
    /**
     * @param <Q>
     * @param list
     * @param empty
     * @return
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static <Q> List<Q> deepCopy(List<Q> list, List<Q> empty) throws IOException, ClassNotFoundException {
        for ( Q each : list) {
            empty.add(Objects.deepCopy(each));
        }
        
        return empty;
    }
    
    /**
     * @param <Q>
     * @param source
     * @param target
     * @return 
     */
    public static <Q> List<Q> intersection(List<Q> source, List<Q> target) {
        final List<Q> rs = new ArrayList<>();
        
        for ( Q each : target) {
            
            if (source.contains(each)) {
                rs.add(each);
            }
            
        }
        
        return rs;
    }
    
    /**
     * @param List
     * @return true if given List is either null or empty. Otherwise false.
     */
    public static boolean nullOrEmpty(List collection) {
        return collection == null || collection.isEmpty();
    }
}
