/*
 * This file is part of terraml-commons project.
 *
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package terraml.commons;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @author Nazım Bahadır Bardakcı - bahadirbardakci@terrayazilim.com.tr
 * @version 1.0.0-SNAPSHOT
 *
 */
public final class Objects {

    private Objects() {
    }

    /**
     * @param <Q>
     * @param object
     * @return
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static <Q> Q deepCopy(Q object) throws IOException, ClassNotFoundException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);

        oos.writeObject(object);
        oos.flush();
        oos.close();
        bos.close();
        byte[] byteData = bos.toByteArray();

        ByteArrayInputStream bais = new ByteArrayInputStream(byteData);

        return (Q) new ObjectInputStream(bais).readObject();
    }

    /**
     * @param Object
     * @return true if given object is not null. Otherwise false.
     */
    public static boolean nonNull(final Object object) {
        return object != null;
    }

    /**
     * @param Object
     * @return true if given objects is null. Otherwise false.
     */
    public static boolean isNull(final Object object) {
        return object == null;
    }

    /**
     * @param Object
     * @return true if given object is array. Otherwise false.
     */
    public static boolean isArray(final Object object) {
        return (object != null) && object.getClass().isArray();
    }

    /**
     * @param Object
     * @param Object
     * @return true if given arguments are equal. Otherwise false.
     */
    public static boolean equals(final Object object0, final Object object1) {
        return (object0 != null) && (object1 != null) && object0.equals(object1);
    }

    /**
     * @param <Q>
     * @param <Q>[]
     *              @
     * param <Q>
     * @return true if given object is contained in the given array. Otherwise false.
     */
    public static <Q> boolean contains(final Q[] array, final Q object) {
        if (!isNull(array) && !isNull(object)) {
            for ( Q each : array ) {
                if (each.equals(object)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param <Q>
     * @param <Q>[]
     *              @
     * param <Q>
     * @return Index of given element in the given array. If given object is not contained in the given array or
     *         given array is either null or empty, then it returns -1.
     */
    public static <Q> int indexOf(final Q[] array, final Q object) {
        if (!isNull(array) && !isNull(object)) {
            final int length = array.length;
            for ( int t = 0; t < length; t++ ) {
                if (array[t].equals(object)) {
                    return t;
                }
            }
        }

        return -1;
    }

    /**
     * @param array
     * @param element
     * @return Object[]
     */
    public static Object[] add(final Object[] array, final Object element) {
        final int length = array.length;
        final Object[] newArray = new Object[length + 1];

        System.arraycopy(array, 0, newArray, 0, length);

        newArray[length] = element;
        return newArray;
    }

    /**
     * @param Object[]
     *                 @
     * return boolean
     */
    public static boolean isEmpty(final Object[] array) {
        return isNull(array) || (array.length == 0);
    }

    /**
     * @param Object[]
     *                 @
     * param int
     * @return Object[]
     */
    public static Object[] remove(final Object[] array, final int index) {
        final int length = array.length;
        if ((index > length) || (index < 0)) {
            throw new IllegalArgumentException("index is invalid");
        }

        final Object[] newArray = new Object[length - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        if (index > length - 1) {
            return newArray;
        }

        final int destination = length - index - 1;
        System.arraycopy(array, index + 1, newArray, index, destination);

        return newArray;
    }

    /**
     * @param Object[]
     *                 @
     * param Object
     * @return Object[]
     */
    public static Object[] remove(final Object[] array, Object element) {
        final int idx = indexOf(array, element);
        if (idx == -1) {
            throw new IllegalArgumentException("there is no such element in the given array.");
        }

        return remove(array, idx);
    }

    /**
     * @param <Q>
     * @param Class
     * @param int
     * @return <Q>[]
     */
    public static <Q> Q[] toArray(final Class reference, final int length) {
        return (Q[]) Array.newInstance(reference.getComponentType(), length);
    }

    /**
     * @param Object
     * @return long
     */
    public static long toLong(final Object object) {
        if (object == null) {
            return 0l;
        }

        Class<?> classType = object.getClass();
        if (classType.getSuperclass().equals(Number.class)) {
            final Number theNumber = (Number) object;
            return theNumber.longValue();
        } else if (object instanceof String) {
            return Long.valueOf(((String) object));
        }

        throw new IllegalArgumentException("Invalid arg");
    }

    /**
     * @param Object
     * @return float
     */
    public static float toFloat(final Object object) {
        if (object == null) {
            return 0f;
        }

        Class<?> classType = object.getClass();
        if (classType.getSuperclass().equals(Number.class)) {
            final Number theNumber = (Number) object;
            return theNumber.floatValue();
        } else if (object instanceof String) {
            return Float.valueOf(((String) object));
        }

        throw new IllegalArgumentException("Invalid arg");
    }

    /**
     * @param Object
     * @return double
     */
    public static double toDouble(final Object object) {
        if (object == null) {
            return 0d;
        }

        Class<?> classType = object.getClass();
        if (classType.getSuperclass().equals(Number.class)) {
            final Number theNumber = (Number) object;
            return theNumber.doubleValue();
        } else if (object instanceof String) {
            return Double.valueOf(((String) object));
        }

        throw new IllegalArgumentException("Invalid arg");
    }

    /**
     * @param Object
     * @return int
     */
    public static int toInt(final Object object) {
        if (object == null) {
            return 0;
        }

        Class<?> classType = object.getClass();
        if (classType.getSuperclass().equals(Number.class)) {
            final Number theNumber = (Number) object;
            return theNumber.intValue();
        } else if (object instanceof String) {
            return Integer.valueOf(((String) object));
        }

        throw new IllegalArgumentException("Invalid arg");
    }

    /**
     * @param Object
     * @return true if given Object is instance of Number. Otherwise false.
     */
    public static boolean isNumber(Object object) {
        return object.getClass().getSuperclass().equals(Number.class);
    }

    /**
     * @param Object
     * @return boolean
     */
    public static boolean toBool(Object object) {
        boolean bool;
        if (object instanceof Boolean) {
            bool = ((boolean) object);
        } else if (object instanceof String) {
            bool = Boolean.valueOf(((String) object));
        } else {
            throw new IllegalArgumentException("Invalid arg");
        }

        return bool;
    }
}
