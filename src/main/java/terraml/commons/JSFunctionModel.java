/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons;

import java.util.SortedMap;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public interface JSFunctionModel {

    /**
     * @return 
     */
    public SortedMap<Integer, Object> getParameters();

    /**
     * @param index
     * @param parameter
     * @return 
     */
    public JSFunctionModel setParameter(int index, Object parameter);
    
    /**
     * @return 
     */
    public String getFunctionName();

    /**
     * @param functionName
     * @return 
     */
    public JSFunctionModel setFunctionName(String functionName);
    
}
