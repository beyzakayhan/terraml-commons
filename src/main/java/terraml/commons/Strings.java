/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons;

import terraml.commons.tuple.LatlonEntry;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @author Nazım Bahadır Bardakcı - bahadirbardakci@terrayazilim.com.tr
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Strings {

    private Strings() {
    }

    /**
     * @param String
     * @return
     */
    public static String inQuote(String string) {
        return new StringBuilder()
                .append("'")
                .append(string)
                .append("'")
                .toString();
    }
    
    /**
     * @param String
     * @return
     */
    public static String inBrackets(String string) {
        return "[" + string + "]";
    }
    
    /**
     * @param String
     * @return
     */
    public static String inCurlyBraces(String string) {
        return "{" + string + "}";
    }
    
    /**
     * @param String
     * @return true if given String is equal to "". Otherwise false.
     */
    public static boolean isBlank(String string) {
        return string.equals("");
    }

    /**
     * @param String
     * @return true if given String is either null or equals to "". Otherwise false.
     */
    public static boolean nullOrBlank(String string) {
        return Objects.isNull(string) || isBlank(string);
    }

    /**
     * @param int
     * @return
     */
    public static String toString(int integer) {
        return String.valueOf(integer);
    }

    /**
     * @param double
     * @return
     */
    public static String toString(double integer) {
        return String.valueOf(integer);
    }

    /**
     * @param boolean
     * @return
     */
    public static String toString(boolean bool) {
        return bool ? "true" : "false";
    }

    /**
     * @param latlonEntry
     * @return
     */
    public static String toString(LatlonEntry latlonEntry) {
        return latlonEntry.lat() + ", " + latlonEntry.lon();
    }

    /**
     * @param String
     * @return
     */
    public static double toDouble(String string) {
        return Double.valueOf(string);
    }

    /**
     * @param String
     * @return
     */
    public static double toInt(String string) {
        return Integer.valueOf(string);
    }

    /**
     * @param String
     * @return
     */
    public static boolean toBool(String string) {
        return Boolean.valueOf(string);
    }

    /**
     * @param String
     * @return Same string without first and last letter of given String.
     */
    public static String clear(String string) {
        return string.substring(1, string.length() - 1);
    }

    /**
     * @param Object
     * @param String
     * @return
     */
    public static String orElse(Object object, String orElse) {
        return (object instanceof String) ? ((String) object) : orElse;
    }
}
