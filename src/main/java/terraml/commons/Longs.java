/*
 * This file is part of terraml-commons project.
 * 
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package terraml.commons;

import java.util.Arrays;
import java.util.stream.LongStream;
import static terraml.commons.Objects.isNull;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @version 1.0.0-SNAPSHOT
 * 
 */
public final class Longs {

    private Longs() {
    }

    /**
     * @param long[]
     * @param long
     * @return
     */
    public static int indexOf(final long[] array, final long element) {
        final int length = array.length;
        for (int t = 0; t < length; t++) {
            if (Long.compare(array[t], element) == 0) {
                return t;
            }
        }

        return -1;
    }

    /**
     * @param long[]
     * @param long
     * @return
     */
    public static boolean contains(final long[] array, final long element) {
        final int length = array.length;
        for (int t = 0; t < length; t++) {
            if (Long.compare(array[t], element) == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param long
     * @return
     */
    public static int findGreatestIndex(final long[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (Long.compare(array[t], array[found]) > 0) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param long[]
     * @return
     */
    public static int findSmallestIndex(final long[] array) {
        int found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = 0;
        boolean isFound = false;
        for (int t = 0; t < length; t++) {
            if (Long.compare(array[t], array[found]) < 0) {
                found = t;
                isFound = true;
            }
        }

        return isFound ? found : -1;
    }

    /**
     * @param long[]
     * @param long
     * @return
     */
    public static long[] add(final long[] array, final long element) {
        final int length = array.length;
        final long[] newArray = new long[length + 1];

        System.arraycopy(array, 0, newArray, 0, length);

        newArray[length] = element;
        return newArray;
    }

    /**
     * @param long[]
     * @return
     */
    public static boolean isEmpty(final long[] array) {
        return isNull(array) || (array.length == 0);
    }

    /**
     * @param long[]
     * @param int
     * @return
     */
    public static long[] remove(final long[] array, final int index) {
        final int length = array.length;
        if ((index > length) || (index < 0)) {
            throw new IllegalArgumentException("index is invalid");
        }

        final long[] newArray = new long[length - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        if (index > length - 1) {
            return newArray;
        }

        final int destination = length - index - 1;
        System.arraycopy(array, index + 1, newArray, index, destination);

        return newArray;
    }

    /**
     * @param long[]
     * @param long
     * @return 
     */
    public static long[] remove(final long[] array, long element) {
        final int idx = indexOf(array, element);
        if (idx == -1) {
            throw new IllegalArgumentException("there is no such element in the given array.");
        }

        return remove(array, idx);
    }

    /**
     * @param long[]
     * @return
     */
    public static long findGreatest(final long[] array) {
        long found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.max(array[t], found);
        }

        return found;
    }

    /**
     * @param long[]
     * @return
     */
    public static long findSmallest(final long[] array) {
        long found = -1;
        if (isNull(array)) {
            return found;
        }

        final int length = array.length;
        found = array[0];
        for (int t = 0; t < length; t++) {
            found = Math.min(array[t], found);
        }

        return found;
    }

    /**
     * @param long[]
     * @return
     */
    public static long[] sort(final long[] source) {
        return LongStream.of(source).sorted().toArray();
    }

    /**
     * @param long[]
     * @param long[]
     * @return
     */
    public static long[] removeAll(final long[] source, final long[] rmv) {
        return Arrays.stream(rmv)
                .filter(each -> !contains(source, each))
                .toArray();
    }

    /**
     * @param long
     * @param long
     * @return
     */
    public static boolean isEqual(final long number0, final long number1) {
        return Long.compare(number0, number1) == 0;
    }

    /**
     * @param long
     * @param long
     * @return
     */
    public static boolean isGreater(final long number0, final long number1) {
        return Long.compare(number0, number1) > 0;
    }

    /**
     * @param long
     * @param long
     * @return 
     */
    public static boolean isGreaterEqual(final long number0, final long number1) {
        return Long.compare(number0, number1) >= 0;
    }

    /**
     * @param long
     * @param long
     * @return
     */
    public static boolean isSmaller(final long number0, final long number1) {
        return Long.compare(number0, number1) < 0;
    }

    /**
     * @param long
     * @param long
     * @return
     */
    public static boolean isSmallerEqual(final long number0, final long number1) {
        return Long.compare(number0, number1) <= 0;
    }

    /**
     * @param long
     * @return
     */
    public static boolean isNegative(final long number0) {
        return Long.compare(number0, 0l) < 0;
    }

    /**
     * @param long
     * @return
     */
    public static boolean isPositive(final long number0) {
        return Long.compare(number0, 0l) > 0;
    }
}
