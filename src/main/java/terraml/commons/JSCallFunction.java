/*
 * This file is part of terraml-commons project.
 *
 * Copyright (C) 2018 Terra Software Informatics LLC. | info@terrayazilim.com.tr
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package terraml.commons;

import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author M.Çağrı Tepebaşılı - cagritepebasili [at] protonmail [dot] com
 * @author Salih Sarı - salihsari@terrayazilim.com.tr
 * @author Berat Eke - berateke@terrayazilim.com.tr
 * @version 1.0.0-SNAPSHOT
 */
public class JSCallFunction implements JSFunctionModel {

    private final SortedMap<Integer, Object> parameters;

    private String functionName;
    private String obj;

    public JSCallFunction() {
        parameters = new TreeMap<>();
    }

    /**
     *
     * @return
     */
    public String getObj() {

        return obj;

    }

    /**
     *
     * @param obj
     * @return
     */
    public JSCallFunction setObj(String obj) {

        this.obj = obj;

        return this;

    }

    /**
     * @return
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * @param functionName
     * @return
     */
    public JSCallFunction setFunctionName(String functionName) {

        this.functionName = functionName;
        return this;

    }

    /**
     * @return
     */
    public SortedMap<Integer, Object> getParameters() {

        return parameters;

    }

    /**
     * @param index
     * @param parameter
     * @return
     */
    public JSCallFunction setParameter(int index, Object parameter) {

        if (parameter != null) {

            if (parameter instanceof String) {
                parameters.put(index, "" + parameter + "");
            } else {
                parameters.put(index, parameter);
            }

        } else {
            parameters.put(index, "null");
        }

        return this;

    }

    /**
     * @return
     */
    public JSBuilder build() {

        return new JSBuilder(this);

    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.functionName);
        hash = 97 * hash + Objects.hashCode(this.parameters);
        hash = 97 * hash + Objects.hashCode(this.obj);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSCallFunction other = (JSCallFunction) obj;
        if (!Objects.equals(this.functionName, other.functionName)) {
            return false;
        }
        if (!Objects.equals(this.obj, other.obj)) {
            return false;
        }
        if (!Objects.equals(this.parameters, other.parameters)) {
            return false;
        }
        return true;
    }
}
